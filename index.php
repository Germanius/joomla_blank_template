<?php
  defined('_JEXEC') or die;
  $app = jFactory::getApplication();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <jdoc:include type="head" />
  <link rel="stylesheet" href="<?= $this->baseurl ?>/templates/system/css/system.css">
  <link rel="stylesheet" href="<?= $this->baseurl ?>/templates/<?= $this->template ?>/css/reset.css">
  <link rel="stylesheet" href="<?= $this->baseurl ?>/templates/<?= $this->template ?>/css/mobile.css">
  <link rel="stylesheet" href="<?= $this->baseurl ?>/templates/<?= $this->template ?>/css/desktop.css">
</head>
<body>
  <h1>Hello, I am J3 Blank template</h1>
  <p>status: working</p>
</body>
</html>